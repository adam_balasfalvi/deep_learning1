﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson04
{
    class BasicNeuralNetwork
    {
        const int inputSize = 4;
        const int hiddenNeuronCount = 3;
        const int outputSize = 1;

        readonly Variable x;
        readonly Variable yt;
        readonly Function y;
        readonly Parameter w1, b, w2;

        public BasicNeuralNetwork()
        {
            // Build graph
            x = Variable.InputVariable(new int[] { inputSize }, DataType.Float);
            w1 = new Parameter(new int[] { hiddenNeuronCount, inputSize }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            b = new Parameter(new int[] { hiddenNeuronCount }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            w2 = new Parameter(new int[] { outputSize, hiddenNeuronCount }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            y = CNTKLib.Sigmoid(CNTKLib.Times(w2, CNTKLib.Sigmoid(CNTKLib.Plus(CNTKLib.Times(w1, x), b))));
        }

        public void Train(string[] trainData)
        {
            int n = trainData.Length;

            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { 1, outputSize }, DataType.Float);
            Function sqDiff = CNTKLib.Square(CNTKLib.Minus(y, yt));
            Function loss = CNTKLib.ReduceSum(sqDiff, Axis.AllAxes());
            Learner learner = CNTKLib.SGDLearner(new ParameterVector() { w1, b, w2 }, new TrainingParameterScheduleDouble(0.01, 1));
            Trainer trainer = Trainer.CreateTrainer(loss, loss, null, new List<Learner>() { learner });

            // Train
            for (int i = 0; i < 1000; i++)
            {
                double sumLoss = 0;
                foreach (string line in trainData)
                {
                    float[] values = line.Split('\t').Select(x => float.Parse(x)).ToArray();
                    //var inputDataMap = new Dictionary<Variable, Value>()
                    //{
                    //    { x, LoadInput(values[0], values[1], values[2], values[3]} },
                    //    {yt, Value.CreateBatch(yt.Shape, new float[] { values[4]}, DeviceDescriptor.CPUDevice) }
                    //};
                    //}
                }
            }

            //public float Prediction(float age, float height, float weight, float salary)
            //{

            //}

            //public LoadInput(float age, float height, float weight, float salary)
            //{

            //}
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            BasicNeuralNetwork bsn = new BasicNeuralNetwork();
        }
    }
}

﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson06
{
    class BatchTraining
    {
        const int hiddenNeuronCount = 4;
        const int epochCount = 2000;
        const int batchSize = 50;
        readonly Variable x;
        readonly Function y;

        public BatchTraining()
        {
            // Build graph
            x = Variable.InputVariable(new int[] { DataSet.InputSize }, DataType.Float, "x"); // bemeno valtozo
            Parameter w1 = new Parameter(new int[] { hiddenNeuronCount, DataSet.InputSize }, DataType.Float, CNTKLib.GlorotNormalInitializer()); // tenzor
            Parameter b = new Parameter(new int[] { hiddenNeuronCount }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            Parameter w2 = new Parameter(new int[] { DataSet.OutputSize, hiddenNeuronCount }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            y = CNTKLib.Sigmoid(CNTKLib.Times(w2, CNTKLib.Sigmoid(CNTKLib.Plus(CNTKLib.Times(w1, x), b))));
        }

        public BatchTraining(string filename)
        {
            y = Function.Load(filename, DeviceDescriptor.CPUDevice);
            x = y.Arguments.First(x => x.Name == "x");
        }

        public void Save(string filename)
        {
            y.Save(filename);
        }

        public void Train(DataSet ds)
        {
            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { DataSet.OutputSize }, DataType.Float);
            Function loss = CNTKLib.BinaryCrossEntropy(y, yt);

            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(0.01, 1));
            Trainer trainer = Trainer.CreateTrainer(y, loss, y_yt_equal, new List<Learner>() { learner });

            // Train
            for (int epochI = 0; epochI <= epochCount; epochI++)
            {
                double sumLoss = 0;
                double sumEval = 0;

                ds.Shuffle();
                for (int batchI = 0; batchI < ds.Count / batchSize; batchI++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * DataSet.InputSize, batchSize * DataSet.InputSize), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * DataSet.OutputSize, batchSize * DataSet.OutputSize), DeviceDescriptor.CPUDevice);

                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        {x, x_value },
                        {yt, yt_value }
                    };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumEval += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(String.Format("{0}\tloss:{1}\teval{2}", epochI, sumLoss / ds.Count, sumEval / ds.Count));
            }
        }

        public double Evaluate(DataSet ds)
        {
            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { DataSet.OutputSize }, DataType.Float);
            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);
            Evaluator evaluator = CNTKLib.CreateEvaluator(y_yt_equal);

            double sumEval = 0;
            for (int batchI = 0; batchI < ds.Count / batchSize; batchI++)
            {
                Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * DataSet.InputSize, batchSize * DataSet.InputSize), DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * DataSet.OutputSize, batchSize * DataSet.OutputSize), DeviceDescriptor.CPUDevice);

                var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        {x, x_value },
                        {yt, yt_value }
                    };

                sumEval += evaluator.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * batchSize;
            }
            return sumEval / ds.Count;
        }

        //public float Prediction()
        //{

        //}
    }   

    public class DataSet
    {
        // Tanitasi minta elofeldolgozasa
        //
        // 1. Cache (memoriaba betoltes)
        // 2. Adatok szetbontasa
        //      input stream
        //      output stream
        // 3. Adatok osszekeverese
        // 4. Adatok normalizalasa (oszloponkent)
        //      (0 - 1 tartomanyba kepezes)
        //      MinMax
        //      f_1(a) = (a - min_1) / (max_1 - min_1)
        // 5. Duplikaciok torlese
        // 6. Kimenetek normalizalasa (kulonbozo kategoria ugyanannyiszor szerepeljen)

        public const int InputSize = 4;
        public List<float> Input { get; set; } = new List<float>();

        public const int OutputSize = 4;
        public List<float> Output { get; set; } = new List<float>();

        public int Count { get; set; }

        public DataSet(string filename)
        {
            LoadData(filename);
        }

        void LoadData(string filename)
        {
            // fajl beolvasasa
            Count = 0;
            foreach (String line in File.ReadAllLines(filename))
            {
                // var floats = Normalize(line.Split('\t').Select(x => float.Parse(x)).ToList());
                // Input.AddRange(floats.GetRange(0, )
            }
        }

        public void Shuffle()
        {
            // keveres
            Random rnd = new Random();
            for (int swapI = 0; swapI < Count; swapI++)
            {

            }
        }

        static float[] minValues;
        static float[] maxValues;

        //public static List<float> Normalize(List<float> floats)
        //{

        //}

        //public void LoadMinMax(string filename)
        //{

        //}
    }

    class Program
    {
        static void Main(string[] args)
        {
            //DataSet.LoadMinMax();
            //DataSet trainDs = new DataSet();
            //DataSet testDs = new DataSet();

            //BatchTraining app = new BatchTraining();
            //app.Train(trainDs);
            //app.Save("");
            //Console.WriteLine("Eval train:");
            //Console.WriteLine("Eval test:");
        }
    }
}

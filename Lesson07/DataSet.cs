﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson07
{
   public class DataSet
    {
        // Tanitasi minta elofeldolgozasa
        //
        // 1. Cache (memoriaba betoltes)
        // 2. Adatok szetbontasa
        //      input stream
        //      output stream
        // 3. Adatok osszekeverese
        // 4. Adatok normalizalasa (oszloponkent)
        //      (0 - 1 tartomanyba kepezes)
        //      MinMax
        //      f_1(a) = (a - min_1) / (max_1 - min_1)
        // 5. Duplikaciok torlese
        // 6. Kimenetek normalizalasa (kulonbozo kategoria ugyanannyiszor szerepeljen)

        public const int InputSize = 4;
        public List<float> Input { get; set; } = new List<float>();
        public List<float> OriginalInput { get; set; } = new List<float>();

        public const int OutputSize = 1;
        public List<float> Output { get; set; } = new List<float>();

        public int Count { get; set; }

        static float[] minValues = new float[InputSize];
        static float[] maxValues = new float[InputSize];

        public DataSet(string filename)
        {
            LoadData(filename);
        }

        void LoadData(string filename)
        {
            // fajl beolvasasa
            Count = 0;
            LoadMinMax(filename);
            foreach (String line in File.ReadAllLines(filename))
            {
                // a normalizalt ertekek hozzaadasa az Inputhoz
                // var floats = Normalize(line.Split('\t').Select(x => float.Parse(x)).ToList());

                String[] stringValues = line.Split('\t');

                // az utolso ertek (fuggo valtozo) leszedese es a tobbi ertek float-ta alakitasa
                List<float> inputValues = stringValues.Take(stringValues.Length - 1).Select(x => float.Parse(x)).ToList();

                // eredeti Input
                OriginalInput.AddRange(inputValues);

                // normalizalas
                List<float> normalizedInputValues = Normalize(inputValues);

                // Inputhoz hozzaadas
                Input.AddRange(normalizedInputValues);

                // a kiertekelt dontes hozzaadasa az Outputhoz
                float outputValue = float.Parse(stringValues.Last());
                Output.Add(outputValue);

                // Count novelese
                Count++;
            }
        }

        public void Shuffle()
        {
            // keveres
            Random rnd = new Random();
            for (int swapI = 0; swapI < Count; swapI++)
            {
                Input.Insert(rnd.Next(0, Count), Input[swapI]);
            }
        }

        public static List<float> Normalize(List<float> floats)
        {
            for (int i = 0; i < InputSize; i++)
            {
                floats[i] = (floats[i] - minValues[i]) / (maxValues[i] - minValues[i]);
            }

            return floats;
        }

        public void LoadMinMax(string filename)
        {
            String[] lines = File.ReadAllLines(filename);
            for (int i = 0; i < lines.Length; i++)
            {
                String[] line = lines[i].Split('\t');

                // elso sor alapjan a default min es max ertekek beallitasa
                if (i == 0)
                {
                    for (int j = 0; j < InputSize; j++)
                    {
                        minValues[j] = float.Parse(line[j]);
                        maxValues[j] = float.Parse(line[j]);
                    }
                }

                // min es max kivalasztas
                for (int k = 0; k < InputSize; k++)
                {
                    float num = float.Parse(line[k]);
                    if (num < minValues[k])
                    {
                        minValues[k] = num;
                    }

                    if (num > maxValues[k])
                    {
                        maxValues[k] = num;
                    }
                }
            }
        }

        public string DataToString(int index)
        {
            List<float> values = OriginalInput.GetRange(index * 4, 4);
            values.Add(Output[index]);

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("{0}", index));
            sb.Append(string.Format("\tAge: {0}", values[0]));
            sb.Append(string.Format("\tHeight: {0}", values[1]));
            sb.Append(string.Format("\tWeight: {0}", values[2]));
            sb.Append(string.Format("\tSalary: {0}", values[3]));
            sb.Append(string.Format("\tDecision: {0}", values[4]));

            return sb.ToString();
        }
    }
}

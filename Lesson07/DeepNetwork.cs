﻿using CNTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson07
{
    class DeepNetwork
    {
        readonly int[] layers = new int[] { DataSet.InputSize, 10, 10, 10, DataSet.OutputSize };

        const int epochCount = 100;
        const int batchSize = 50;
        readonly Variable x;
        readonly Function y;

        public DeepNetwork()
        {
            // Build graph
            x = Variable.InputVariable(new int[] { layers[0] }, DataType.Float, "x"); // bemeno valtozo

            Function lastLayer = x;
            for (int i = 0; i < layers.Length - 1; i++)
            {
                Parameter weight = new Parameter(new int[] { layers[i + 1], layers[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer()); // tenzor
                Parameter bias = new Parameter(new int[] { layers[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                Function times = CNTKLib.Times(weight, lastLayer);
                Function plus = CNTKLib.Plus(times, bias);
                lastLayer = CNTKLib.Sigmoid(plus);
            }

            y = lastLayer;
        }

        // load
        public DeepNetwork(string filename)
        {
            y = Function.Load(filename, DeviceDescriptor.CPUDevice);
            x = y.Arguments.First(x => x.Name == "x");
        }

        // save
        public void Save(string filename)
        {
            y.Save(filename);
        }

        public void Train(DataSet ds)
        {
            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { DataSet.OutputSize }, DataType.Float);
            Function loss = CNTKLib.BinaryCrossEntropy(y, yt);

            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(0.01, 1));
            Trainer trainer = Trainer.CreateTrainer(y, loss, y_yt_equal, new List<Learner>() { learner });

            // Train
            for (int epochI = 0; epochI <= epochCount; epochI++)
            {
                double sumLoss = 0;
                double sumEval = 0;

                // ds.Shuffle();
                for (int batchI = 0; batchI < ds.Count / batchSize; batchI++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * DataSet.InputSize, batchSize * DataSet.InputSize), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * DataSet.OutputSize, batchSize * DataSet.OutputSize), DeviceDescriptor.CPUDevice);

                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        {x, x_value },
                        {yt, yt_value }
                    };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumEval += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(String.Format("{0}\tloss: {1}\teval: {2}", epochI, sumLoss / ds.Count, sumEval / ds.Count));
            }
        }

        public double Evaluate(DataSet ds)
        {
            // Extend graph
            // a teszt adatok kimenete
            Variable yt = Variable.InputVariable(new int[] { DataSet.OutputSize }, DataType.Float);

            // a betanitott halozat kimenetenek a kerekitese
            Function y_rounded = CNTKLib.Round(y);

            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);
            Evaluator evaluator = CNTKLib.CreateEvaluator(y_yt_equal);

            double sumEval = 0;
            for (int batchI = 0; batchI < ds.Count / batchSize; batchI++)
            {
                Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * DataSet.InputSize, batchSize * DataSet.InputSize), DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * DataSet.OutputSize, batchSize * DataSet.OutputSize), DeviceDescriptor.CPUDevice);

                var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        {x, x_value },
                        {yt, yt_value }
                    };

                sumEval += evaluator.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * batchSize;
            }

            Console.WriteLine("Evaluation on test data: {0}", sumEval / ds.Count);
            return sumEval / ds.Count;
        }

        public float Prediction()
        {
            return 0;
        }

        public string VisibleTest(DataSet ds, int count)
        {
            Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(0, count * DataSet.InputSize), DeviceDescriptor.CPUDevice);
            var inputDataMap = new UnorderedMapVariableValuePtr()
            {
                {x, x_value }
            };
            var outputDataMap = new UnorderedMapVariableValuePtr()
            {
                {y, null }
            };

            y.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
            IList<IList<float>> resultValue = outputDataMap[y].GetDenseData<float>(y);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++)
            {
                sb.Append(ds.DataToString(i));
                sb.Append(string.Format("\tPrediction: {0}\n", Math.Round(resultValue[i][0])));
            }

            return sb.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DataSet trainDataSet = new DataSet("..\\..\\..\\..\\data\\HusbandEvaluation.txt");
            DataSet testDataSet = new DataSet("..\\..\\..\\..\\data\\HusbandEvaluation-test.txt");
            DeepNetwork deepNetwork = new DeepNetwork();

            deepNetwork.Train(trainDataSet);
            deepNetwork.Evaluate(testDataSet);

            Console.WriteLine(deepNetwork.VisibleTest(testDataSet, 10));

            Console.ReadLine();
        }
    }
}

﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson09
{
    class MNISTClassification
    {
        

        const int epochCount = 50;
        const int batchSize = 100;
        readonly Variable x;
        readonly Function y;

        public MNISTClassification(int hiddenNeuronCount)
        {
            int[] layers = new int[] { MNISTDataSet.InputSize, hiddenNeuronCount, hiddenNeuronCount, MNISTDataSet.OutputSize };

            x = Variable.InputVariable(new int[] { layers[0] }, DataType.Float, "x"); // bemeno valtozo

            Function lastLayer = x;
            for (int i = 0; i < layers.Length - 1; i++)
            {
                Parameter weight = new Parameter(new int[] { layers[i + 1], layers[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer()); // tenzor
                Parameter bias = new Parameter(new int[] { layers[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                Function times = CNTKLib.Times(weight, lastLayer);
                Function plus = CNTKLib.Plus(times, bias);

                // ha nem az utolso reteg
                if (i != layers.Length - 2)
                {
                    lastLayer = CNTKLib.Sigmoid(plus);
                }
                // ha utolso reteg
                else
                {
                    lastLayer = CNTKLib.Softmax(plus);
                }               
            }

            y = lastLayer;
        }

        public void Train(MNISTDataSet ds)
        {
            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { MNISTDataSet.OutputSize }, DataType.Float);
            Function loss = CNTKLib.CrossEntropyWithSoftmax(y, yt);
            Function err = CNTKLib.ClassificationError(y, yt);

            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(1, batchSize));
            Trainer trainer = Trainer.CreateTrainer(y, loss, y_yt_equal, new List<Learner>() { learner });

            // Train
            for (int epochI = 0; epochI <= epochCount; epochI++)
            {
                double sumLoss = 0;
                double sumEval = 0;

                // ds.Shuffle();
                for (int batchI = 0; batchI < ds.Count / batchSize; batchI++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * MNISTDataSet.InputSize, batchSize * MNISTDataSet.InputSize), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * MNISTDataSet.OutputSize, batchSize * MNISTDataSet.OutputSize), DeviceDescriptor.CPUDevice);

                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        {x, x_value },
                        {yt, yt_value }
                    };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumEval += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(String.Format("{0}\tloss: {1:0.000}\teval: {2:0.000}\terr: {3:0.000}", epochI, sumLoss / ds.Count, sumEval / ds.Count, 1.0 - sumEval / ds.Count));
            }
        }

        public void Evaluate(MNISTDataSet ds, out double lossValue, out double accValue)
        {
            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { MNISTDataSet.OutputSize }, DataType.Float);
            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);
            Function loss = CNTKLib.CrossEntropyWithSoftmax(y, yt);
            Function err = CNTKLib.ClassificationError(y, yt);

            Evaluator evaluator_loss = CNTKLib.CreateEvaluator(loss);
            Evaluator evaluator_err = CNTKLib.CreateEvaluator(err);

            double sumEval = 0;
            double sumLoss = 0;
            for (int batchI = 0; batchI < ds.Count / batchSize; batchI++)
            {
                Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * MNISTDataSet.InputSize, batchSize * MNISTDataSet.InputSize), DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * MNISTDataSet.OutputSize, batchSize * MNISTDataSet.OutputSize), DeviceDescriptor.CPUDevice);

                var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        {x, x_value },
                        {yt, yt_value }
                    };

                sumLoss += evaluator_loss.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * batchSize;
                sumEval += evaluator_err.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * batchSize;
            }

            lossValue = sumLoss / ds.Count;
            accValue = 1 - sumEval / ds.Count;
        }

        public String VisibleTest(MNISTDataSet ds, int count)
        {
            Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(0, count * MNISTDataSet.InputSize), DeviceDescriptor.CPUDevice);
            var inputDataMap = new UnorderedMapVariableValuePtr()
            {
                {x, x_value }
            };
            var outputDataMap = new UnorderedMapVariableValuePtr()
            {
                { y, null}
            };

            y.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
            IList<IList<float>> resultValue = outputDataMap[y].GetDenseData<float>(y);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                sb.Append(ds.DataToString(i));
                sb.Append("\nResult:[");
                int max = 0;
                for (int d = 0; d <= 9; d++)
                {
                    if (resultValue[i][d] > resultValue[i][max])
                    {
                        max = d;
                    }
                    if (d != 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(String.Format("{0:0.00}", resultValue[i][d]));

                }
                sb.Append("]\nPrediction:").Append(max).Append("\n\n");
            }
            return sb.ToString();
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            int hiddenNeuronCount = 10;

            MNISTDataSet trainDs = new MNISTDataSet("..\\..\\..\\..\\data\\mnist\\train-labels.idx1-ubyte", "..\\..\\..\\..\\data\\mnist\\train-images.idx3-ubyte");
            MNISTDataSet testDs = new MNISTDataSet("..\\..\\..\\..\\data\\mnist\\t10k-labels.idx1-ubyte", "..\\..\\..\\..\\data\\mnist\\t10k-images.idx3-ubyte");           

            MNISTClassification app = new MNISTClassification(hiddenNeuronCount);
            app.Train(trainDs);
            app.Evaluate(trainDs, out double trainLoss, out double trainAcc);
            app.Evaluate(testDs, out double testLoss, out double testAcc);
            Console.WriteLine(String.Format(
                "Final evaluation: hiddenNeuronCount: {0}\t" +
                "trainLoss: {1:0.000}\t" +
                "trainAcc: {2:0.000}\t" +
                "testLoss: {3:0.000}\t" +
                "testAcc: {4:0.000}",
                hiddenNeuronCount,
                trainLoss,
                trainAcc,
                testLoss,
                testAcc
                )
            );
            Console.WriteLine(app.VisibleTest(trainDs, 10));
            Console.ReadLine();
        }
    }
}

